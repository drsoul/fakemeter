<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WriteOffInspect extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'write_off_inspects';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['user_vk_id', 'inspects'];
}
