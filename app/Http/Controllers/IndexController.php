<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $this->data['scripts'][] = 'all.js';
        //return Response->setStatusCode(201, 'The resource is created successfully!');
        //return response('You need to login again.', 401);
        return response()->view('index/index', $this->data)->header('200 OK', '');
    }
}
