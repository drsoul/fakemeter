<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\UserVk;
use App\WriteOffInspect;
use Request as RequestFasade;

class InspectsController extends Controller
{

    protected $start_inspects = 5;
    protected $payInspectsForOneOperation = 1;
    protected $request;

    public function __construct(\Illuminate\Http\Request $request)
    {
        if(!RequestFasade::ajax()){
            echo "Error not ajax.";
            die();
        }
        
        $this->request = $request;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getInstpects()
    {

        $input = $this->request->all();
        if(empty($input['user_vk_id'])){
          $this->errorNotSendUserId();
        }

        $userVk = UserVk::firstOrNew(['user_vk_id' => $input['user_vk_id']]);

        $save_result = false;
        if(empty($userVk->created_at)){
          $userVk->inspects = $this->start_inspects;
          $userVk->user_vk_id = $input['user_vk_id'];
          $save_result = $userVk->save();
        }

        if(empty($userVk->created_at) AND !$save_result){
          $this->errorNotValidUserId();
        }

        $response['error'] = 0;
        $response['error_msg'] = '';
        $response['response']['user_vk_id'] = $userVk->user_vk_id;
        $response['response']['inspects'] = $userVk->inspects;
        echo json_encode($response);
        die();
    }

    public function writeOffInstpects(){
      $input = $this->request->all();
      if(empty($input['user_vk_id'])){
        $this->errorNotSendUserId();
      }

      $userVk  = UserVk::where(['user_vk_id' => $input['user_vk_id']])->first();
      if($userVk == null){
        $this->errorNotValidUserId();
      }
      if($userVk->inspects < 1){
        $this->errorNotEnoughInspects();
      }

      //log write-off
      $writeOffInspect = new WriteOffInspect;
      $writeOffInspect->inspects = $this->payInspectsForOneOperation;
      $writeOffInspect->user_vk_id = $userVk->user_vk_id;
      if(!$writeOffInspect->save()){
        $this->errorPaymentFail($userVk);
      }

      //write-off inspect from user balance
      $userVk->inspects = $userVk->inspects - $this->payInspectsForOneOperation;
      if($userVk->save()){
        $this->successPayment($userVk);
      }else{
        $this->errorPaymentFail($userVk);
      }
    }

    protected function successPayment($userVk){
      $response['error'] = 0;
      $response['error_msg'] = '';
      $response['response']['user_vk_id'] = $userVk->user_vk_id;
      $response['response']['inspects'] = $userVk->inspects;
      $response['response']['payment_status'] = 'success';
      echo json_encode($response);
      die();
    }

    protected function errorPaymentFail($userVk){
      $response['error'] = 4;
      $response['error_msg'] = 'Payment fail, repeat operation.';
      $response['response']['user_vk_id'] = $userVk->user_vk_id;
      $response['response']['inspects'] = $userVk->inspects;
      $response['response']['payment_status'] = 'fail';
      echo json_encode($response);
      die();
    }
    protected function errorNotSendUserId(){
      $response['error'] = 2;
      $response['error_msg'] = 'Not send user id';
      $response['response'] = [];
      echo json_encode($response);
      die();
    }

    protected function errorNotValidUserId(){
      $response['error'] = 2;
      $response['error_msg'] = 'Not valid user id';
      $response['response'] = [];
      echo json_encode($response);
      die();
    }

    protected function errorNotEnoughInspects(){
      $response['error'] = 3;
      $response['error_msg'] = 'Not enough inspects (balance).';
      $response['response'] = [];
      echo json_encode($response);
      die();
    }
}
