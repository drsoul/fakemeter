<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\OrderNotice;
use App\UserVk;
use App\Order;


class PaymentsController extends Controller
{


	protected $secret_key = 'Cb5iR6Hv6y4n3mpJMMKP';
	protected $request;

	public function __construct(\Illuminate\Http\Request $request)
	{
	   	//$this->request = $request;
	}

	protected function displayItemAndDie($item){

		$response = $this->itemsStore($item);
		if(!$response){
			$this->dipslayErrorAndDie(20, 'Товара не существует.', true);
		}
		echo json_encode($response['for_json_response']);
		die();
	}

	protected function dipslayErrorAndDie($error_code, $error_msg, $critical){
		$response['error'] = array(
			'error_code' => $error_code,
			'error_msg' => $error_msg,
			'critical' => $critical
		);

		echo json_encode($response);
		die();
	}

	protected function itemsStore($item){

		if ($item === 'item10') {
			return $this->itemFrame(10, '10 проверок', 10, 10);
		} elseif ($item === 'item30') {
			return $this->itemFrame(30, '30 проверок (скидка 33%)', 20, 30);
		}elseif ($item === 'item50') {
			return $this->itemFrame(50, '50 проверок (скидка 40%)', 30, 50);
		}elseif ($item === 'item300') {
			return $this->itemFrame(300, '300 проверок (скидка 50%)', 150, 300);
		}else{
			return false;
		}
	}

	protected function itemFrame($item_id, $title, $price, $inspects,
	$photo_url = 'http://fakemer.ru/img/search_icon.png'){

		$response['for_json_response']['response'] = array(
			'item_id' => $item_id,
			'title' => $title,
			'photo_url' => $photo_url,
			'price' => $price
		);
		$response['inspects'] = $inspects;
		return $response;
	}

	protected function displayOrderIdAdDie($order_id, $app_order_id){
		$response['response'] = array(
			'order_id' => $order_id,
			'app_order_id' => $app_order_id,
		);
		echo json_encode($response);
		die();
	}

	protected function orderStatusChange($input){
		// Изменение статуса заказа в тестовом режиме
		if ($input['status'] == 'chargeable') {

			$orderNotice = new OrderNotice;
			$orderNotice->notification_type = $input['notification_type'];
			$orderNotice->app_id = $input['app_id'];
			$orderNotice->user_vk_id = $input['user_id'];
			$orderNotice->receiver_id = $input['receiver_id'];
			$orderNotice->order_id = $input['order_id'];
			$orderNotice->date = $input['date'];
			$orderNotice->status = $input['status'];
			$orderNotice->sig = $input['sig'];
			$orderNotice->item = $input['item'];
			$orderNotice->item_id = (isset($input['item_id']))?$input['item_id']:0;
			$orderNotice->item_title = $input['item_title'];
			$orderNotice->item_photo_url = $input['item_photo_url'];
			$orderNotice->item_price = $input['item_price'];
			$orderNotice->save();
			$orderNotice->id;

			$order = Order::select('*')
			->where('order_id', '=', $input['order_id'])->first();

			if(isset($order->id)){
				#return old order
				$this->displayOrderIdAdDie($order->order_id, $order->id);
			}else{
				if($orderNotice->id < 1){
					$this->dipslayErrorAndDie(2,
					'Ошибка БД. Повторите попытку позже.', false);
				}

				if(strpos($input['item'], 'item') !== false){
					$item = $this->itemsStore($input['item']);
					$inspects = $item['inspects'];
					if(!$inspects){
						$this->dipslayErrorAndDie(20, 'Товара не существует.', true);
					}
				}else{
					//$inspects это проверки т.е. денежные единицы приложения
					$inspects = $input['item_price'];
				}

				$userVk = UserVk::firstOrCreate(['user_vk_id' => $input['receiver_id']]);

				if(isset($userVk->user_vk_id)){
					#create new order
					$order = new Order;
					$order->order_id = $input['order_id'];
					$order->order_notice_id = $orderNotice->id;
					$order->user_vk_id = $userVk->user_vk_id;
					$order->inspects = $inspects;
					$order->save();
					#return new order
					if($order->id){
						UserVk::where('user_vk_id', $userVk->user_vk_id)
		          ->increment('inspects', $inspects);

						$this->displayOrderIdAdDie($order->order_id, $order->id);
					}else{
						$this->dipslayErrorAndDie(2,
						'Ошибка БД. Повторите попытку позже.', false);
					}
				}else{
					$this->dipslayErrorAndDie(2,
					'Ошибка БД. Повторите попытку позже.', false);
				}
			}
		} else {
			$this->dipslayErrorAndDie(100,
			'Передано непонятно что вместо chargeable.', true);
		}
	}

	protected function makeInputStr($input){
		$sig = $input['sig'];
		unset($input['sig']);
		ksort($input);
		$str = '';
		foreach ($input as $k => $v) {
			$str .= $k.'='.$v;
		}
		$res['str'] = $str;
		$res['sig'] = $sig;
		return $res;
	}

	public function index(Request $request){

		header("Content-Type: application/json; encoding=utf-8");
		$secret_key = $this->secret_key; // Защищенный ключ приложения
		//$input = $this->request->all();
		$input = $_POST;
		// Проверка подписи
		if(empty($input['sig'])){
			$this->dipslayErrorAndDie(1, 'Не передан sig.', true);
		}

		$makeInputStrRes = $this->makeInputStr($input);
		$sig = $makeInputStrRes['sig'];
		$str = $makeInputStrRes['str'];

		//echo md5($str.$secret_key);

		if ($sig != md5($str.$secret_key)) {
			$this->dipslayErrorAndDie(10,
			'Несовпадение вычисленной и переданной подписи запроса.', true);
		} else {
			// Подпись правильная
			switch ($input['notification_type']) {
				case 'get_item':
					// Получение информации о товаре
					$this->displayItemAndDie($input['item']);
				break;

				case 'get_item_test':
					// Получение информации о товаре в тестовом режиме
					$this->displayItemAndDie($input['item']);
				break;

				case 'order_status_change':
					// Изменение статуса заказа
					$this->orderStatusChange($input);
				break;

				case 'order_status_change_test':
					$this->orderStatusChange($input);
			  break;
			}
		}
	}
}
