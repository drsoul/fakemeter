<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@Index');
//Route::get('/getVkProfile', 'ParserController@getVkProfile');
Route::match(['get', 'post'], '/payments', 'PaymentsController@Index');
Route::match(['get', 'post'], '/get_inspects', 'InspectsController@getInstpects');
Route::match(['get', 'post'], '/write_off_inspects', 'InspectsController@writeOffInstpects');
