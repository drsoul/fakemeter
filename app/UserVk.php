<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVk extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'users_vk';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['user_vk_id', 'inspects'];
}
