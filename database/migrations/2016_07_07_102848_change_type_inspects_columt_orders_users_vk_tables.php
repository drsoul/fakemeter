<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeInspectsColumtOrdersUsersVkTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::table('orders', function ($table) {
           $table->bigInteger('inspects')->change();
       });
       Schema::table('users_vk', function ($table) {
           $table->bigInteger('inspects')->change();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::table('orders', function ($table) {
           $table->string('inspects')->change();
       });
       Schema::table('users_vk', function ($table) {
           $table->string('inspects')->change();
       });
     }
}
