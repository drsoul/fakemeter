<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('order_notices', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->bigInteger('order_notice_id');
          $table->string('notification_type');
          $table->bigInteger('app_id');
          $table->bigInteger('user_vk_id');
          $table->bigInteger('receiver_id');
          $table->bigInteger('order_id');
          $table->string('date');
          $table->string('status');
          $table->string('sig');
          $table->string('item');
          $table->bigInteger('item_id');
          $table->string('item_title');
          $table->string('item_photo_url');
          $table->bigInteger('item_price');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('order_notices');
    }
}
