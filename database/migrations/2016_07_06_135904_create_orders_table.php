<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('orders', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->bigInteger('order_id');
           $table->bigInteger('order_notice_id');
           $table->bigInteger('user_vk_id');
           $table->string('inspects');
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::drop('orders');
     }
}
