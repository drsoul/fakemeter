<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWriteOffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('write_off_inspects', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->bigInteger('user_vk_id');
           $table->string('inspects');
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::drop('write_off_inspects');
     }
}
