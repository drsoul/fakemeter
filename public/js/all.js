//import dom from '/js/app/dom.js';
//import analitics from '/js/app/analitics.js';

'use strict';

var analitics = {

    getCommonFakePercent: function getCommonFakePercent() {
        return new Promise(function (resolve, reject) {
            dom.analTO(function () {
                try {
                    var acceptFriendshipPercent = dom.i(dom.ls('analitics.acceptFriendshipPercent'));
                    var fakeRatioByFriendProportion = dom.f(dom.ls('analitics.fakeRatioByFriendProportion'));
                    var fixRatio = 0.25;

                    dom.log('analitics.fakeRatioByFriendProportion: ' + fakeRatioByFriendProportion);
                    dom.log('analitics.acceptFriendshipPercent: ' + acceptFriendshipPercent);

                    var commonFakePercent = acceptFriendshipPercent * fakeRatioByFriendProportion * fixRatio;

                    dom.log('commonFakePercent: ' + commonFakePercent);

                    commonFakePercent = dom.i(commonFakePercent);

                    dom.log('commonFakePercent in fn: ' + commonFakePercent);

                    if (commonFakePercent > 99) {
                        commonFakePercent = 99;
                    } else if (commonFakePercent < 1) {
                        commonFakePercent = 1;
                    }

                    if (commonFakePercent <= 40) {
                        var commonFakeMsg = '<h1 class="cover-heading"><span style="color:green;font-weight:bold;">реальный человек</span></h1>';
                        dom.ls('analitics.commonFakeMsg', commonFakeMsg);
                        analitics.commonFakeMsg = commonFakeMsg;
                    } else if (commonFakePercent > 45 && commonFakePercent <= 55) {
                        var commonFakeMsg = '<h1 class="cover-heading"><span style="color:yellow;font-weight:bold;">сомнительно</span></h1>';
                        dom.ls('analitics.commonFakeMsg', commonFakeMsg);
                        analitics.commonFakeMsg = commonFakeMsg;
                    } else if (commonFakePercent > 60) {
                        var commonFakeMsg = '<h1 class="cover-heading"><span style="color:red;font-weight:bold;">фейк</span></h1>';
                        dom.ls('analitics.commonFakeMsg', commonFakeMsg);
                        analitics.commonFakeMsg = commonFakeMsg;
                    }

                    dom.ls('analitics.commonFakePercent', commonFakePercent);
                    dom.log('analitics.commonFakePercent in LS analitics: ', commonFakePercent);

                    analitics.commonFakePercent = commonFakePercent;
                    dom.log('analitics.commonFakePercent in analitics: ', analitics.commonFakePercent);
                    dom.log('analitics.commonFakePercent: ' + dom.ls('analitics.commonFakePercent'));
                    dom.log('analitics.commonFakeMsg: ' + dom.ls('analitics.commonFakeMsg'));

                    resolve();
                } catch (err) {
                    reject(err);
                }
            });
        });
    },

    getFakeRatioByFriendSexProportion: function getFakeRatioByFriendSexProportion() {
        if (dom.ls('user.sex') == '2') {
            dom.ls('analitics.fakeRatioByFriendProportion', dom.ls('friends.w') / dom.ls('friends.m'));
        } else if (dom.ls('user.sex') == '1') {
            dom.ls('analitics.fakeRatioByFriendProportion', dom.ls('friends.m') / dom.ls('friends.w'));
        } else {
            dom.ls('analitics.fakeRatioByFriendProportion', '1');
        }
    },

    getPercentOfUserAccepOfferFriendship: function getPercentOfUserAccepOfferFriendship() {

        var friends = dom.ls('friends');
        var followers = dom.ls('followers');

        dom.log('getPercentOfUserAccepOfferFriendship friends: ' + friends);
        dom.log('getPercentOfUserAccepOfferFriendship followers: ' + followers);
        friends = parseInt(friends);
        followers = parseInt(followers);
        var summ = friends + followers;
        var ratio = friends / summ;
        var percent = dom.i(ratio * 100);
        if (percent > 99) {
            percent = 99;
        } else if (percent < 1) {
            percent = 1;
        }

        dom.ls('analitics.acceptFriendshipPercent', percent);
    }

};

//import dom from '/js/app/dom.js';
//import user from '/js/app/user.js';

var core = {

    //serverUrl: '//fakemeter.local',
    serverUrl: '',

    writeOffInstpects: function writeOffInstpects() {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: core.serverUrl + "/write_off_inspects?user_vk_id=" + user.currentUser.uid
            }).done(function (data) {
                var data = JSON.parse(data);
                if (data.error > 0) {
                    if (data.error === 3) {
                        dom.err(core.i18n(1003));
                        reject(data);
                        return;
                    }
                    dom.log(data.error_msg);
                    $(dom.cssIspectsScoreboard).html(data.error_msg);
                    $(dom.cssIspectsWrapper).show();
                    reject(data);
                } else {
                    if (data.response.payment_status === 'success') {
                        $(dom.cssIspectsScoreboard).html(data.response.inspects);
                        $(dom.cssIspectsWrapper).show();
                        resolve();
                    } else {
                        $(dom.cssIspectsScoreboard).html('Unknown error.');
                        $(dom.cssIspectsWrapper).show();
                        reject();
                    }
                }
            }).error(function (err) {
                dom.log(err);
                $(dom.cssIspectsScoreboard).html("Error Ajax.");
                $(dom.cssIspectsWrapper).show();
                reject(err);
            });
        });
    },

    displayInspects: function displayInspects() {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: core.serverUrl + "/get_inspects?user_vk_id=" + user.currentUser.uid
            }).done(function (data) {
                var data = JSON.parse(data);
                if (data.error > 0) {
                    dom.log(data.error_msg);
                    $(dom.cssIspectsScoreboard).html(data.error_msg);
                    $(dom.cssIspectsWrapper).show();
                    reject(data);
                } else {
                    $(dom.cssIspectsScoreboard).html(data.response.inspects);
                    $(dom.cssIspectsWrapper).show();
                    resolve();
                }
            }).error(function (err) {
                dom.log(err);
                $(dom.cssIspectsScoreboard).html("Error Ajax.");
                $(dom.cssIspectsWrapper).show();
                reject(err);
            });
        });
    },
    prepareSearch: function prepareSearch() {
        var id = dom.elem(dom.cssResearchProfile).val();
        user.idOrAlias = id;
    },
    i18n: function i18n(errorCode, lang) {
        if (lang == null) {
            lang = 'ru';
        }
        var i18n = { 'ru': {
                '113': 'Неверный id пользователя или псевдоним страницы!',
                '1003': 'На балансе не достаточно проверок!'
            }
        };
        return i18n[lang][errorCode];
    }
};

//import vkApi from '/js/app/vkApi.js';

var dom = {

    consoleLog: 0,
    pTimeout: 2000,
    vkTimeout: 100,
    analTimeout: 10,

    cssResearchStart: '#research_start',
    cssResearchProfile: '#research_profile',
    cssResearchResult: '#research_result',
    cssAddAppLeftMenu: '#add_app_left_menu',
    cssCallbacks: '#callbacks',
    cssBuyAnalysis: '#buy_analysis',
    cssSelectBuy: '#select_buy_modal',
    cssBtnBuy: '.btn-buy',
    cssShowOffersButton: '#show_offers_button',
    cssIspectsScoreboard: '#inspects',
    cssIspectsWrapper: '#inspects_wrapper',
    cssInviteFriends: '#invite_friends',

    cssBtnBuyAttrBuyId: 'buy_id',

    loadIcon: '<img src="/img/load_cross.gif">',

    addListeners: function addListeners() {

        $(document).on('click', dom.cssResearchStart, function () {
            dom.log('FAKEMETER CLICK SEARCH');
            dom.elem(dom.cssResearchStart).attr("disabled", "disabled");
            dom.elem(dom.cssResearchResult).html(dom.loadIcon);

            core.prepareSearch();
            vkApi.init();
        });

        /* buy login
        $(document).on('click', dom.cssBtnBuy, function(){
            var buy_id = dom.elem(this).attr(dom.cssBtnBuyAttrBuyId);
            //alert(buy_id);
             dom.log('click cssBtnBuy:'+buy_id);
            vkApi.showOrderBox(buy_id);
            //dom.showSelectBuyModal();
        });
        
        $(document).on('click', dom.cssShowOffersButton, function(){
            vkApi.showOfferBox();
        });
        */

        $(document).on('click', dom.cssInviteFriends, function () {
            VK.callMethod("showInviteBox");
        });
    },

    showSelectBuyModal: function showSelectBuyModal() {
        //dom.elem(dom.cssSelectBuy);
    },

    log: function log() {
        if (this.consoleLog == 1) {
            console.log(Array.prototype.slice.call(arguments));
        }
    },

    elem: function elem(cssSel) {
        return $(cssSel);
    },

    getFnName: function getFnName(fn) {
        return fn.toString().match(/function ([^(]*)\(/)[1];
    },

    ls: function ls(index, value) {

        if (value == null) {
            return window.localStorage[index];
        } else {
            window.localStorage[index] = value;
        }
    },

    lsClear: function lsClear() {
        localStorage.clear();
    },

    i: function i(str) {
        return parseInt(str);
    },

    f: function f(str) {
        return parseFloat(str);
    },

    err: function err(err_msg) {
        dom.elem(dom.cssResearchResult).html('<p style="color:red;">' + err_msg + '</p>');
        dom.clearLSAndActivButtonSearch();
        //throw 'Error';
    },

    vkTO: function vkTO(fn) {
        setTimeout(fn, dom.vkTimeout);
    },

    analTO: function analTO(fn) {
        setTimeout(fn, dom.analTimeout);
    },

    pTO: function pTO(fn) {
        setTimeout(fn, dom.pTimeout);
    },

    clearLSAndActivButtonSearch: function clearLSAndActivButtonSearch() {
        dom.lsClear();
        dom.elem(dom.cssResearchStart).removeAttr("disabled");
    }
};

//import dom from '/js/app/dom.js';
//import vkApi from '/js/app/vkApi.js';

$(document).ready(function () {
    user.requestCurrentUser().then(function () {
        dom.log('FAKEMETER START APPLICATION');
        dom.addListeners();

        vkApi.showAddLeftMenuButton();

        //core.displayInspects();
    });
});

var resp = {

    mansCount: 0,
    womansCount: 0,
    friendsCount: 0

};

var init = {};

//import dom from '/js/app/dom.js';

var user = {

    idOrAlias: '',
    currentUser: '',
    //id: 0,//16302170,
    requestCurrentUser: function requestCurrentUser() {
        return new Promise(function (resolve, reject) {
            VK.api("users.get", {}, function (data) {
                user.currentUser = data.response['0'];
                resolve();
            });
        });
    },
    getId: function getId() {
        return dom.ls('user.id');
    },

    getFN: function getFN() {
        return dom.ls('user.first_name');
    },

    getLN: function getLN() {
        return dom.ls('user.last_name');
    }

};

//import dom from '/js/app/dom.js';
//import user from '/js/app/user.js';

var vkApi = {

    init: function init() {

        VK.init(function () {
            // API initialization succeeded
            // Your code here
            dom.log('VK init success!');

            //just for log
            vkApi.usersSearch(1);
            vkApi.usersSearch(2);
            vkApi.usersSearch(0);

            vkApi.getUserId().then(function () {
                dom.pTO(function () {
                    Promise.all([vkApi.getFollowers(), vkApi.getFriends(), vkApi.getUsers()]).then(getParamsThenAnalizeAndDisplayResult)['catch'](function (err) {
                        dom.log(err);
                    });
                });
            })['catch'](function (err) {
                dom.log(err);
            });
        }, function () {
            // API initialization failed
            // Can reload page here
            dom.log('VK init failed!');
        }, '5.52'); //5.34

        function getParamsThenAnalizeAndDisplayResult() {

            dom.pTO(function () {
                dom.log('window.localStorage["followers"]: ' + window.localStorage['followers']);
                dom.log('window.localStorage["friends"]: ' + window.localStorage['friends']);
                dom.log('window.localStorage["friends.m"]: ' + window.localStorage['friends.m']);
                dom.log('window.localStorage["friends.w"]: ' + window.localStorage['friends.w']);
                dom.log('window.localStorage["user.sex"]: ' + window.localStorage['user.sex']);

                analitics.getFakeRatioByFriendSexProportion();
                analitics.getPercentOfUserAccepOfferFriendship();
                analizeAndDisplayResult();
            });
        }

        function analizeAndDisplayResult() {

            dom.pTO(function () {
                dom.log('analitics.fakeRatioByFriendProportion out of fn: ' + dom.ls('analitics.fakeRatioByFriendProportion'));
                dom.log('analitics.acceptFriendshipPercent out of fn: ' + dom.ls('analitics.acceptFriendshipPercent'));

                analitics.getCommonFakePercent().then(displayAnalResult)['catch'](function (err) {
                    dom.log(err);
                });
            });
        }

        function displayAnalResult() {
            /* payment write-off logic
            core.writeOffInstpects().then(function(){
              dom.elem(dom.cssResearchResult).html(
                  'ID: '+user.getId()
                  +'<br>'
                  +user.getFN()+' '+user.getLN()
                  +'<br>'
                  +'Фейковость: '+analitics.commonFakePercent+'%'
                  +'<br>'
                  +analitics.commonFakeMsg
              );
              dom.clearLSAndActivButtonSearch();
            }).catch(function(){
              dom.clearLSAndActivButtonSearch();
            });
            */

            dom.elem(dom.cssResearchResult).html('ID: ' + user.getId() + '<br>' + user.getFN() + ' ' + user.getLN() + '<br>' + 'Фейковость: ' + analitics.commonFakePercent + '%' + '<br>' + analitics.commonFakeMsg);
            dom.clearLSAndActivButtonSearch();
        }
    },

    getUsers: function getUsers() {
        return new Promise(function (resolve, reject) {
            dom.vkTO(function () {

                dom.log('before getUsers user.id: ' + dom.ls('user.id'));

                VK.api("users.get", { user_ids: user.getId(), fields: 'sex' }, function (data) {

                    try {
                        dom.log(' getUsers user.id: ' + user.getId());
                        dom.log(data);
                        dom.ls('user.sex', data.response[0].sex);
                        resolve();
                    } catch (err) {
                        dom.err(data[0].error['error_msg']);
                        dom.clearLSAndActivButtonSearch();
                        reject(err);
                    }
                });
            });
        });
    },

    getUserId: function getUserId() {
        return new Promise(function (resolve, reject) {
            dom.vkTO(function () {

                if (user.idOrAlias == '') {
                    dom.err(core.i18n(113));
                    dom.clearLSAndActivButtonSearch();
                    reject();
                    return;
                }

                VK.api("users.get", { user_ids: user.idOrAlias }, function (data) {

                    try {
                        dom.log('getUserId: ');
                        dom.log(data);

                        dom.ls('user.id', data.response[0].id);
                        dom.ls('user.first_name', data.response[0].first_name);
                        dom.ls('user.last_name', data.response[0].last_name);
                        resolve();
                    } catch (err) {
                        dom.err(core.i18n(data.error['error_code']));
                        dom.clearLSAndActivButtonSearch();
                        reject(err);
                    }
                });
            });
        });
    },

    getFollowers: function getFollowers() {
        return new Promise(function (resolve, reject) {
            dom.vkTO(function () {

                VK.api("users.getFollowers", { user_id: user.getId() }, function (data) {

                    dom.log('users.getFollowers data: ');
                    dom.log(data);

                    try {
                        var count = data.response['count'];
                        dom.log('count followers: ' + count);
                        dom.ls('followers', count);
                        resolve();
                    } catch (err) {
                        dom.err(data[0].error['error_msg']);
                        dom.clearLSAndActivButtonSearch();
                        reject(err);
                    }
                });
            });
        });
    },

    getFriends: function getFriends() {
        return new Promise(function (resolve, reject) {
            dom.vkTO(function () {

                VK.api("friends.get", { user_id: user.getId(), fields: "sex" }, function (data) {

                    try {
                        var friendsCount = data.response['count'];
                        var womansCount = 0;
                        var mansCount = 0;
                        var key = 0;
                        for (key in data.response['items']) {

                            if (data.response['items'][key]['sex'] == 2) {
                                mansCount++;
                            } else if (data.response['items'][key]['sex'] == 1) {
                                womansCount++;
                            }
                        }

                        dom.ls('friends', friendsCount);
                        dom.ls('friends.w', womansCount);
                        dom.ls('friends.m', mansCount);

                        resolve();
                    } catch (err) {
                        dom.err(data[0].error['error_msg']);
                        dom.clearLSAndActivButtonSearch();
                        reject(err);
                    }
                });
            });
        });
    },

    //нужен запрос прав
    usersSearch: function usersSearch(sex) {

        dom.vkTO(function () {

            VK.api("users.search", { user_id: user.getId(), sex: sex, from_list: "friends" }, function (data) {

                try {
                    var count = data.response['count'];
                    dom.log('count usersSearch: ' + count);
                } catch (err) {
                    dom.err(data[0].error['error_msg']);
                    dom.clearLSAndActivButtonSearch();
                }
            });
        });
    },

    showSettingsBox: function showSettingsBox(bitMap) {

        dom.vkTO(function () {

            VK.api('getUserSettings', function (data) {

                if (data.response) {
                    if (!(bitMap & data.response)) {
                        VK.callMethod('showSettingsBox', bitMap);
                    }
                }

                if (data.error) {
                    alert('Error Code:' + data.error.error);
                }
            });
        });
    },

    showOrderBox: function showOrderBox(buy_id) {

        dom.log('buy_id: ' + buy_id);

        if (buy_id == '' || buy_id == null) {
            return false;
        }

        var params = {
            type: 'item',
            item: 'item' + buy_id
        };

        dom.log('params: ' + params);

        VK.callMethod('showOrderBox', params);

        var callbacksElem = dom.elem(dom.cssCallbacks);

        VK.addCallback('onOrderSuccess', function (order_id) {
            dom.log('EVENT - onOrderSuccess');
            core.displayInspects();
            //callbacksElem.html('<br />Заказ номер: '+order_id+' - удачно выполнен.');
        });
        VK.addCallback('onOrderFail', function () {
            dom.log('EVENT - onOrderFail');
            //callbacksElem.html('<br />Заказ не выполнен.');
        });
        VK.addCallback('onOrderCancel', function () {
            dom.log('EVENT - onOrderCancel');
            //callbacksElem.html('<br />Заказ отменён.');
        });
    },

    showOfferBox: function showOfferBox() {

        var params = {
            type: 'offers',
            currency: 1
        };

        dom.log('params: ' + params);

        VK.callMethod('showOrderBox', params);

        var callbacksElem = dom.elem(dom.cssCallbacks);

        VK.addCallback('onOrderSuccess', function (order_id) {
            core.displayInspects();
            dom.log('EVENT - onOrderSuccess - Offer');
            //callbacksElem.html('<br />Заказ номер: '+order_id+' - удачно выполнен.');
        });
        VK.addCallback('onOrderFail', function () {
            dom.log('EVENT - onOrderFail - Offer');
            //callbacksElem.html('<br />Заказ не выполнен.');
        });
        VK.addCallback('onOrderCancel', function () {
            dom.log('EVENT - onOrderCancel - Offer');
            //callbacksElem.html('<br />Заказ отменён.');
        });
    },

    showAddLeftMenuButton: function showAddLeftMenuButton() {

        dom.vkTO(function () {

            VK.api('getUserSettings', function (data) {

                if (data.response) {
                    if (!(256 & data.response)) {

                        $(document).on('click', dom.cssAddAppLeftMenu, function () {
                            vkApi.showSettingsBox(256);
                        });

                        dom.elem(dom.cssAddAppLeftMenu).show();
                    }
                }

                if (data.error) {
                    alert('Error Code:' + data.error.error);
                }
            });
        });
    }
};
//# sourceMappingURL=all.js.map
