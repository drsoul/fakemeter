import dom from '/js/app/dom.js';

var user = {

    idOrAlias: '',
    //id: 0,//16302170,
    getId: function(){
        return dom.ls('user.id');
    },

    getFN: function(){
        return dom.ls('user.first_name');
    },

    getLN: function(){
        return dom.ls('user.last_name');
    }

}
