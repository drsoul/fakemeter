import vkApi from '/js/app/vkApi.js';

var dom = {

    consoleLog: 1,
    pTimeout: 2000,
    vkTimeout: 100,
    analTimeout: 10,

    cssResearchStart: '#research_start',
    cssResearchProfile: '#research_profile',
    cssResearchResult: '#research_result',
    cssAddAppLeftMenu: '#add_app_left_menu',
    cssCallbacks: '#callbacks',
    cssBuyAnalysis: '#buy_analysis',
    cssSelectBuy: '#select_buy_modal',
    cssBtnBuy: '.btn-buy',
    cssShowOffersButton: '#show_offers_button',

    cssBtnBuyAttrBuyId: 'buy_id',

    loadIcon: '<img src="/img/load_cross.gif">',

    addListeners: function(){


        $(document).on('click', dom.cssResearchStart, function(){
            dom.log('FAKEMETER CLICK SEARCH');
            dom.elem(dom.cssResearchStart).attr("disabled", "disabled");
            dom.elem(dom.cssResearchResult).html(dom.loadIcon);

            Promise.all([core.prepareSearch()]).then(function(){

                Promise.all([vkApi.init()]).then(function(){

                });
            });

        });

        $(document).on('click', dom.cssBtnBuy, function(){
            var buy_id = dom.elem(this).attr(dom.cssBtnBuyAttrBuyId);
            //alert(buy_id);

            dom.log('click cssBtnBuy:'+buy_id);
            vkApi.showOrderBox(buy_id);
            //dom.showSelectBuyModal();
        });

        $(document).on('click', dom.cssShowOffersButton, function(){
            vkApi.showOfferBox();
        });

    },

    showSelectBuyModal: function(){
        //dom.elem(dom.cssSelectBuy);
    },

    log: function(msg){
        if(this.consoleLog == 1){
            console.log(msg);
        }
    },

    elem: function(cssSel){
        return $(cssSel);
    },

    getFnName: function(fn) {
        return fn.toString().match(/function ([^(]*)\(/)[1];
    },

    ls: function(index, value){

        if(value == null){
            return window.localStorage[index];
        }else{
            window.localStorage[index] = value;
        }
    },

    lsClear:function(){
        localStorage.clear();
    },

    i: function(str){
        return parseInt(str);
    },

    f: function(str){
        return parseFloat(str);
    },

    err: function(err_msg){
        dom.elem(dom.cssResearchResult).html('<p style="color:red;">'+err_msg+'</p>');
        throw 'Error';
    },

    vkTO: function(fn){
        setTimeout(fn, dom.vkTimeout);
    },

    analTO: function(fn){
        setTimeout(fn, dom.analTimeout);
    }

}
