import dom from '/js/app/dom.js';
import user from '/js/app/user.js';

var vkApi = {

    init: function(){

        VK.init(function() {
             // API initialization succeeded
             // Your code here
            dom.log('VK init success!');

            vkApi.usersSearch(1);
            vkApi.usersSearch(2);
            vkApi.usersSearch(0);

            Promise.all([vkApi.getUserId()]).then(function(){

                setTimeout(function(){

                    Promise.all([vkApi.getFollowers(), vkApi.getFriends(), vkApi.getUsers()]).then(function(){

                        setTimeout(function(){

                            dom.log('window.localStorage["followers"]: '+window.localStorage['followers']);
                            dom.log('window.localStorage["friends"]: '+window.localStorage['friends']);
                            dom.log('window.localStorage["friends.m"]: '+window.localStorage['friends.m']);
                            dom.log('window.localStorage["friends.w"]: '+window.localStorage['friends.w']);
                            dom.log('window.localStorage["user.sex"]: '+window.localStorage['user.sex']);

                            Promise.all([

                                analitics.getFakeRatioByFriendSexProportion(),
                                analitics.getPercentOfUserAccepOfferFriendship()

                            ]).then(function(){

                                setTimeout(function(){

                                    dom.log('analitics.fakeRatioByFriendProportion out of fn: '+dom.ls('analitics.fakeRatioByFriendProportion'));
                                    dom.log('analitics.acceptFriendshipPercent out of fn: '+dom.ls('analitics.acceptFriendshipPercent'));


                                    Promise.all([analitics.getCommonFakePercent()]).then(function(){

                                        setTimeout(function(){

                                            Promise.all([
                                                dom.elem(dom.cssResearchResult).html(
                                                    'ID: '+user.getId()
                                                    +'<br>'
                                                    +user.getFN()+' '+user.getLN()
                                                    +'<br>'
                                                    //+'Фейковость: '+dom.ls('analitics.commonFakePercent')+'%'
                                                    +'Фейковость: '+analitics.commonFakePercent+'%'
                                                    +'<br>'
                                                    +analitics.commonFakeMsg
                                                    //+dom.ls('analitics.commonFakeMsg')
                                                )
                                            ]).then(function(){

                                                setTimeout(function(){

                                                    dom.lsClear();
                                                    dom.elem(dom.cssResearchStart).removeAttr("disabled");

                                                }, dom.pTimeout);
                                            });

                                        }, dom.pTimeout+500);

                                    });

                                }, dom.pTimeout);


                                dom.log('dom.ls("friends.r"): '+dom.ls('friends.r'));

                                dom.log('analitics.commonFakePercent: '+dom.ls('analitics.commonFakePercent'));

                                dom.log('analitics.commonFakeMsg: '+dom.ls('analitics.commonFakeMsg'));
                            });

                        }, dom.pTimeout);
                    });

                }, dom.pTimeout);

            });




        }, function() {
             // API initialization failed
             // Can reload page here
             dom.log('VK init failed!');
        }, '5.52');  //5.34

    },

    getUsers: function(){

        dom.vkTO(function(){

            dom.log('before getUsers user.id: '+dom.ls('user.id'));

            VK.api("users.get", {user_ids:user.getId(), fields: 'sex'}, function(data) {

                try{
                    dom.log(' getUsers user.id: '+user.getId());
                    dom.log(data);

                    dom.ls('user.sex', data.response[0].sex);
                }catch(err){
                    dom.err(data.error['error_msg']);
                }

            });
        });
    },

    getUserId: function(){

        dom.vkTO(function(){

            VK.api("users.get", {user_ids:user.idOrAlias}, function(data) {

                try{
                    dom.log('getUserId: ');
                    dom.log(data);

                    dom.ls('user.id', data.response[0].id);
                    dom.ls('user.first_name', data.response[0].first_name);
                    dom.ls('user.last_name', data.response[0].last_name);
                }catch(err){
                    dom.err(data.error['error_msg']);
                }

            });

        });
    },

    getFollowers: function(){

        dom.vkTO(function(){

            VK.api("users.getFollowers", {user_id:user.getId()}, function(data) {

                dom.log('users.getFollowers data: ');
                dom.log(data);

                try{
                    var count = data.response['count'];
                    dom.log('count followers: '+count);

                    dom.ls('followers', count);
                }catch(err){
                    dom.err(data.error['error_msg']);
                }

            });

        });
    },

    getFriends: function(){

        dom.vkTO(function(){

            VK.api("friends.get", {user_id:user.getId(), fields: "sex"}, function(data) {

                try{
                    var friendsCount = data.response['count'];
                    var womansCount = 0;
                    var mansCount = 0;
                    for(key in data.response['items'] ){

                        if(data.response['items'][key]['sex'] == 2){
                            mansCount++;
                        }else if(data.response['items'][key]['sex'] == 1){
                            womansCount++;
                        }
                    }

                    dom.ls('friends', friendsCount);
                    dom.ls('friends.w', womansCount);
                    dom.ls('friends.m', mansCount);
                }catch(err){
                    dom.err(data.error['error_msg']);
                }

            });

        });
    },

    //нужен запрос прав
    usersSearch: function(sex){

        dom.vkTO(function(){

            VK.api("users.search",
            {user_id: user.getId(), sex: sex, from_list: "friends"},
            function(data) {

                try{
                    var count = data.response['count'];
                    dom.log('count usersSearch: '+count);
                }catch(err){
                    dom.err(data.error['error_msg']);
                }
            });

        });
    },

    showSettingsBox: function(bitMap){

        dom.vkTO(function(){

            VK.api('getUserSettings', function(data){

                if (data.response){
                    if (!(bitMap & data.response)){
                        VK.callMethod('showSettingsBox', bitMap);
                    }
                }

                if (data.error){
                    alert('Error Code:'+data.error.error);
                }

            });

        });
    },

    showOrderBox: function(buy_id){

        dom.log('buy_id: '+buy_id);

        if(buy_id == '' || buy_id == null){
            return false;
        }

        var params = {
          type: 'item',
          item: 'item'+buy_id
        };

        dom.log('params: '+params);

        VK.callMethod('showOrderBox', params);

        var callbacksElem = dom.elem(dom.cssCallbacks);

        VK.addCallback('onOrderSuccess', function(order_id) {
            callbacksElem.val('<br />Заказ номер: '+order_id+' - удачно выполнен.');
        });
        VK.addCallback('onOrderFail', function() {
            callbacksElem.val('<br />Заказ не выполнен.');
        });
        VK.addCallback('onOrderCancel', function() {
            callbacksElem.val('<br />Заказ отменён.');
        });
    },

    showOfferBox: function(){

        var params = {
          type: 'offers',
          currency: 1
        };

        dom.log('params: '+params);

        VK.callMethod('showOrderBox', params);

        var callbacksElem = dom.elem(dom.cssCallbacks);

        VK.addCallback('onOrderSuccess', function(order_id) {
            callbacksElem.val('<br />Заказ номер: '+order_id+' - удачно выполнен.');
        });
        VK.addCallback('onOrderFail', function() {
            callbacksElem.val('<br />Заказ не выполнен.');
        });
        VK.addCallback('onOrderCancel', function() {
            callbacksElem.val('<br />Заказ отменён.');
        });
    },

    showAddLeftMenuButton: function(){


        dom.vkTO(function(){

            VK.api('getUserSettings', function(data){



                if (data.response){
                    if (!(256 & data.response)){

                        $(document).on('click', dom.cssAddAppLeftMenu, function(){
                            vkApi.showSettingsBox(256);
                        });

                        dom.elem(dom.cssAddAppLeftMenu).show();
                    }
                }

                if (data.error){
                    alert('Error Code:'+data.error.error);
                }

            });

        });
    }


}
