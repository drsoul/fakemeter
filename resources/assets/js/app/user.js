//import dom from '/js/app/dom.js';

var user = {

    idOrAlias: '',
    currentUser: '',
    //id: 0,//16302170,
    requestCurrentUser: function(){
      return new Promise(function(resolve, reject){
        VK.api("users.get", {}, function(data){          
          user.currentUser = data.response['0'];
          resolve();
        });
      });
    },
    getId: function(){
        return dom.ls('user.id');
    },

    getFN: function(){
        return dom.ls('user.first_name');
    },

    getLN: function(){
        return dom.ls('user.last_name');
    }


}
