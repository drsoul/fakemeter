//import dom from '/js/app/dom.js';
//import user from '/js/app/user.js';

var vkApi = {

    init: function(){

        VK.init(function() {
             // API initialization succeeded
             // Your code here
            dom.log('VK init success!');

            //just for log
            vkApi.usersSearch(1);
            vkApi.usersSearch(2);
            vkApi.usersSearch(0);

            vkApi.getUserId().then(function(){
                dom.pTO(function(){
                    Promise.all([vkApi.getFollowers(),
                    vkApi.getFriends(), vkApi.getUsers()])
                    .then(getParamsThenAnalizeAndDisplayResult)
                    .catch(function(err){
                      dom.log(err);
                    });
                });
            }).catch(function(err){
              dom.log(err);
            });

        }, function() {
             // API initialization failed
             // Can reload page here
             dom.log('VK init failed!');
        }, '5.52');  //5.34

        function getParamsThenAnalizeAndDisplayResult(){

            dom.pTO(function(){
                dom.log('window.localStorage["followers"]: '+window.localStorage['followers']);
                dom.log('window.localStorage["friends"]: '+window.localStorage['friends']);
                dom.log('window.localStorage["friends.m"]: '+window.localStorage['friends.m']);
                dom.log('window.localStorage["friends.w"]: '+window.localStorage['friends.w']);
                dom.log('window.localStorage["user.sex"]: '+window.localStorage['user.sex']);

                analitics.getFakeRatioByFriendSexProportion();
                analitics.getPercentOfUserAccepOfferFriendship();
                analizeAndDisplayResult();
            });
        }

        function analizeAndDisplayResult(){

            dom.pTO(function(){
                dom.log('analitics.fakeRatioByFriendProportion out of fn: '
                +dom.ls('analitics.fakeRatioByFriendProportion'));
                dom.log('analitics.acceptFriendshipPercent out of fn: '
                +dom.ls('analitics.acceptFriendshipPercent'));

                analitics.getCommonFakePercent()
                .then(displayAnalResult)
                .catch(function(err){dom.log(err);});
            });

        }

        function displayAnalResult(){
            /* payment write-off logic
            core.writeOffInstpects().then(function(){
              dom.elem(dom.cssResearchResult).html(
                  'ID: '+user.getId()
                  +'<br>'
                  +user.getFN()+' '+user.getLN()
                  +'<br>'
                  +'Фейковость: '+analitics.commonFakePercent+'%'
                  +'<br>'
                  +analitics.commonFakeMsg
              );
              dom.clearLSAndActivButtonSearch();
            }).catch(function(){
              dom.clearLSAndActivButtonSearch();
            });
            */

            dom.elem(dom.cssResearchResult).html(
                'ID: '+user.getId()
                +'<br>'
                +user.getFN()+' '+user.getLN()
                +'<br>'
                +'Фейковость: '+analitics.commonFakePercent+'%'
                +'<br>'
                +analitics.commonFakeMsg
            );
            dom.clearLSAndActivButtonSearch();
        }
    },

    getUsers: function(){
        return new Promise(function(resolve, reject){
            dom.vkTO(function(){

                dom.log('before getUsers user.id: '+dom.ls('user.id'));

                VK.api("users.get", {user_ids:user.getId(), fields: 'sex'}, function(data) {

                    try{
                        dom.log(' getUsers user.id: '+user.getId());
                        dom.log(data);
                        dom.ls('user.sex', data.response[0].sex);
                        resolve();
                    }catch(err){
                        dom.err(data[0].error['error_msg']);
                        dom.clearLSAndActivButtonSearch();
                        reject(err);
                    }
                });
            });
        });
    },

    getUserId: function(){
        return new Promise(function(resolve, reject){
            dom.vkTO(function(){

              if(user.idOrAlias == ''){
                dom.err(core.i18n(113));
                dom.clearLSAndActivButtonSearch();
                reject();
                return;
              }

              VK.api("users.get", {user_ids:user.idOrAlias}, function(data) {

                  try{
                      dom.log('getUserId: ');
                      dom.log(data);

                      dom.ls('user.id', data.response[0].id);
                      dom.ls('user.first_name', data.response[0].first_name);
                      dom.ls('user.last_name', data.response[0].last_name);
                      resolve();
                  }catch(err){
                      dom.err(core.i18n(data.error['error_code']));
                      dom.clearLSAndActivButtonSearch();
                      reject(err);
                  }
              });
            });
          }
        );
    },

    getFollowers: function(){
        return new Promise(function(resolve, reject){
            dom.vkTO(function(){

                VK.api("users.getFollowers", {user_id:user.getId()}, function(data) {

                    dom.log('users.getFollowers data: ');
                    dom.log(data);

                    try{
                        var count = data.response['count'];
                        dom.log('count followers: '+count);
                        dom.ls('followers', count);
                        resolve();
                    }catch(err){
                        dom.err(data[0].error['error_msg']);
                        dom.clearLSAndActivButtonSearch();
                        reject(err);
                    }
                });
            });
        });
    },

    getFriends: function(){
      return new Promise(function(resolve, reject){
        dom.vkTO(function(){

            VK.api("friends.get", {user_id:user.getId(), fields: "sex"}, function(data) {

                try{
                    var friendsCount = data.response['count'];
                    var womansCount = 0;
                    var mansCount = 0;
                    var key = 0;
                    for(key in data.response['items']){

                        if(data.response['items'][key]['sex'] == 2){
                            mansCount++;
                        }else if(data.response['items'][key]['sex'] == 1){
                            womansCount++;
                        }
                    }

                    dom.ls('friends', friendsCount);
                    dom.ls('friends.w', womansCount);
                    dom.ls('friends.m', mansCount);

                    resolve();
                }catch(err){
                    dom.err(data[0].error['error_msg']);
                    dom.clearLSAndActivButtonSearch();
                    reject(err);
                }
            });
        });
      });
    },

    //нужен запрос прав
    usersSearch: function(sex){

        dom.vkTO(function(){

            VK.api("users.search",
            {user_id: user.getId(), sex: sex, from_list: "friends"},
            function(data) {

                try{
                    var count = data.response['count'];
                    dom.log('count usersSearch: '+count);
                }catch(err){
                    dom.err(data[0].error['error_msg']);
                    dom.clearLSAndActivButtonSearch();
                }
            });
        });
    },

    showSettingsBox: function(bitMap){

        dom.vkTO(function(){

            VK.api('getUserSettings', function(data){

                if (data.response){
                    if (!(bitMap & data.response)){
                        VK.callMethod('showSettingsBox', bitMap);
                    }
                }

                if (data.error){
                    alert('Error Code:'+data.error.error);
                }
            });
        });
    },

    showOrderBox: function(buy_id){

        dom.log('buy_id: '+buy_id);

        if(buy_id == '' || buy_id == null){
            return false;
        }

        var params = {
          type: 'item',
          item: 'item'+buy_id
        };

        dom.log('params: '+params);

        VK.callMethod('showOrderBox', params);

        var callbacksElem = dom.elem(dom.cssCallbacks);

        VK.addCallback('onOrderSuccess', function(order_id) {
            dom.log('EVENT - onOrderSuccess');
            core.displayInspects();
            //callbacksElem.html('<br />Заказ номер: '+order_id+' - удачно выполнен.');
        });
        VK.addCallback('onOrderFail', function() {
            dom.log('EVENT - onOrderFail');
            //callbacksElem.html('<br />Заказ не выполнен.');
        });
        VK.addCallback('onOrderCancel', function() {
            dom.log('EVENT - onOrderCancel');
            //callbacksElem.html('<br />Заказ отменён.');
        });
    },

    showOfferBox: function(){

        var params = {
          type: 'offers',
          currency: 1
        };

        dom.log('params: '+params);

        VK.callMethod('showOrderBox', params);

        var callbacksElem = dom.elem(dom.cssCallbacks);

        VK.addCallback('onOrderSuccess', function(order_id) {
            core.displayInspects();
            dom.log('EVENT - onOrderSuccess - Offer');
            //callbacksElem.html('<br />Заказ номер: '+order_id+' - удачно выполнен.');
        });
        VK.addCallback('onOrderFail', function() {
            dom.log('EVENT - onOrderFail - Offer');
            //callbacksElem.html('<br />Заказ не выполнен.');
        });
        VK.addCallback('onOrderCancel', function() {
            dom.log('EVENT - onOrderCancel - Offer');
            //callbacksElem.html('<br />Заказ отменён.');
        });
    },

    showAddLeftMenuButton: function(){

        dom.vkTO(function(){

            VK.api('getUserSettings', function(data){

                if (data.response){
                    if (!(256 & data.response)){

                        $(document).on('click', dom.cssAddAppLeftMenu, function(){
                            vkApi.showSettingsBox(256);
                        });

                        dom.elem(dom.cssAddAppLeftMenu).show();
                    }
                }

                if (data.error){
                    alert('Error Code:'+data.error.error);
                }
            });
        });
    }
}
