//import dom from '/js/app/dom.js';
//import user from '/js/app/user.js';

var core = {

   //serverUrl: '//fakemeter.local',
   serverUrl: '',

   writeOffInstpects: function(){
     return new Promise(function(resolve, reject){
       $.ajax({
           url: core.serverUrl+"/write_off_inspects?user_vk_id="+user.currentUser.uid,
       }).done(function(data) {
           var data = JSON.parse(data);
           if(data.error > 0){
             if(data.error === 3){
               dom.err(core.i18n(1003));
               reject(data);
               return;
             }
             dom.log(data.error_msg);
             $(dom.cssIspectsScoreboard).html(data.error_msg);
             $(dom.cssIspectsWrapper).show();
             reject(data);
           }else{
             if(data.response.payment_status === 'success'){
               $(dom.cssIspectsScoreboard).html(data.response.inspects);
               $(dom.cssIspectsWrapper).show();
               resolve();
             }else{
               $(dom.cssIspectsScoreboard).html('Unknown error.');
               $(dom.cssIspectsWrapper).show();
               reject();
             }
           }
       }).error(function(err){
         dom.log(err);
         $(dom.cssIspectsScoreboard).html("Error Ajax.");
         $(dom.cssIspectsWrapper).show();
         reject(err);
       });
     });
   },

   displayInspects: function(){
      return new Promise(function(resolve, reject){
        $.ajax({
            url: core.serverUrl+"/get_inspects?user_vk_id="+user.currentUser.uid,
        }).done(function(data) {
            var data = JSON.parse(data);
            if(data.error > 0){
              dom.log(data.error_msg);
              $(dom.cssIspectsScoreboard).html(data.error_msg);
              $(dom.cssIspectsWrapper).show();
              reject(data);
            }else{
              $(dom.cssIspectsScoreboard).html(data.response.inspects);
              $(dom.cssIspectsWrapper).show();
              resolve();
            }
        }).error(function(err){
          dom.log(err);
          $(dom.cssIspectsScoreboard).html("Error Ajax.");
          $(dom.cssIspectsWrapper).show();
          reject(err);
        });
      });
   },
   prepareSearch: function(){
       var id = dom.elem(dom.cssResearchProfile).val();
       user.idOrAlias = id;
   },
   i18n: function(errorCode, lang){
     if(lang == null){
       lang = 'ru';
     }
     var i18n = {'ru':
        {
          '113':'Неверный id пользователя или псевдоним страницы!',
          '1003':'На балансе не достаточно проверок!'
        },
      };
     return i18n[lang][errorCode];
   }
}
