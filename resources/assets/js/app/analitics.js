//import dom from '/js/app/dom.js';
//import analitics from '/js/app/analitics.js';

var analitics = {

    getCommonFakePercent: function(){
      return new Promise(function(resolve, reject){
        dom.analTO(function(){
            try{
              var acceptFriendshipPercent = dom.i(dom.ls('analitics.acceptFriendshipPercent'));
              var fakeRatioByFriendProportion = dom.f(dom.ls('analitics.fakeRatioByFriendProportion'));
              var fixRatio = 0.25;

              dom.log('analitics.fakeRatioByFriendProportion: '+fakeRatioByFriendProportion);
              dom.log('analitics.acceptFriendshipPercent: '+acceptFriendshipPercent);

              var commonFakePercent = acceptFriendshipPercent  * fakeRatioByFriendProportion * fixRatio;

              dom.log('commonFakePercent: '+commonFakePercent);

              commonFakePercent = dom.i(commonFakePercent);

              dom.log('commonFakePercent in fn: '+commonFakePercent);

              if(commonFakePercent > 99){
                  commonFakePercent = 99;
              }else if(commonFakePercent < 1){
                  commonFakePercent = 1;
              }

              if(commonFakePercent <= 40){
                  var commonFakeMsg = '<h1 class="cover-heading"><span style="color:green;font-weight:bold;">реальный человек</span></h1>';
                  dom.ls('analitics.commonFakeMsg', commonFakeMsg);
                  analitics.commonFakeMsg = commonFakeMsg;
              }else if(commonFakePercent > 45 && commonFakePercent <= 55){
                  var commonFakeMsg = '<h1 class="cover-heading"><span style="color:yellow;font-weight:bold;">сомнительно</span></h1>';
                  dom.ls('analitics.commonFakeMsg', commonFakeMsg);
                  analitics.commonFakeMsg = commonFakeMsg;
              }else if(commonFakePercent > 60){
                  var commonFakeMsg = '<h1 class="cover-heading"><span style="color:red;font-weight:bold;">фейк</span></h1>';
                  dom.ls('analitics.commonFakeMsg', commonFakeMsg);
                  analitics.commonFakeMsg = commonFakeMsg;
              }

              dom.ls('analitics.commonFakePercent', commonFakePercent);
              dom.log('analitics.commonFakePercent in LS analitics: ', commonFakePercent);

              analitics.commonFakePercent = commonFakePercent;
              dom.log('analitics.commonFakePercent in analitics: ', analitics.commonFakePercent);
              dom.log('analitics.commonFakePercent: '
              +dom.ls('analitics.commonFakePercent'));
              dom.log('analitics.commonFakeMsg: '
              +dom.ls('analitics.commonFakeMsg'));

              resolve();
            }catch(err){
              reject(err);
            }
        });
      });
    },

    getFakeRatioByFriendSexProportion: function(){
        if(dom.ls('user.sex') == '2'){
            dom.ls('analitics.fakeRatioByFriendProportion',  dom.ls('friends.w') / dom.ls('friends.m'));
        }else if(dom.ls('user.sex') == '1'){
            dom.ls('analitics.fakeRatioByFriendProportion',  dom.ls('friends.m') / dom.ls('friends.w'));
        }else{
            dom.ls('analitics.fakeRatioByFriendProportion', '1');
        }
    },

    getPercentOfUserAccepOfferFriendship: function(){

        var friends = dom.ls('friends');
        var followers = dom.ls('followers');

        dom.log('getPercentOfUserAccepOfferFriendship friends: '+friends);
        dom.log('getPercentOfUserAccepOfferFriendship followers: '+followers);
        friends = parseInt(friends);
        followers = parseInt(followers);
        var summ = friends + followers;
        var ratio = friends / summ;
        var percent = dom.i(ratio * 100);
        if(percent > 99){
            percent = 99;
        }else if(percent < 1){
            percent = 1;
        }

        dom.ls('analitics.acceptFriendshipPercent', percent);

    }

}
