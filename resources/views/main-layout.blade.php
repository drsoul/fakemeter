<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Фейкомер</title>

    <!-- <script src="/js/jquery-2.1.1.js"  type="text/javascript">
    </script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Morris -->
    <link href="css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <!-- <link href="css/style.css" rel="stylesheet"> -->
    
    <script src="//vk.com/js/api/xd_connection.js?2"  type="text/javascript">
    </script>

    <link href="css/cover.css" rel="stylesheet">

</head>

<body>

@yield('content')


{{--Юзерские скрипты, подключаются если есть переменная--}}
@if(isset($scripts))
    @foreach($scripts as $script)
        <script src="/js/{{$script}}"></script>
    @endforeach
@endif

{{--Юзерские стили, подключаются если есть переменная--}}
@if(isset($styles))
    @foreach($styles as $style)
        <link rel="stylesheet" href="/css/{{$style}}">
    @endforeach
@endif

</body>
</html>
