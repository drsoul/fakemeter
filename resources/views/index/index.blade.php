@extends('main-layout')

@section('content')

<style>
/* select_buy_modal */
.packs-block td{
  padding: 10px;
  width:250px;
  margin:auto;
}

#select_buy_modal{
  color:#3e3e3e;
}
.pack-popular{
  color:#54af54;
  font-weight: bold;
  text-shadow:none;
}
.pack-benefit{
  color:#ffe700;
  font-weight: bold;
  text-shadow:none;
}
.searching_money{
  width:24px;
}
input{
  color:#333;
  padding:5px;
  width:200pt;
}
/* Определение основной анимации */

@keyframes colours {
  /*
  0% {
    -moz-box-shadow:0 0 30px #78ff78;
    -webkit-box-shadow:0 0 30px #78ff78;
    box-shadow:0 0 30px #78ff78;
  }

  25% {
    -moz-box-shadow:0 0 40px #68e168;
    -webkit-box-shadow:0 0 40px #68e168;
    box-shadow:0 0 40px #68e168;
  }

  50% {
    -moz-box-shadow:0 0 50px #54af54;
    -webkit-box-shadow:0 0 50px #54af54;
    box-shadow:0 0 50px #54af54;
  }

  75% {
    -moz-box-shadow:0 0 40px #68e168;
    -webkit-box-shadow:0 0 40px #68e168;
    box-shadow:0 0 40px #68e168;
  }
  */


  0% {
    -moz-box-shadow:0 0 20px #FFD700;
    -webkit-box-shadow:0 0 20px #FFD700;
    box-shadow:0 0 20px #FFD700;
  }

  25% {
    -moz-box-shadow:0 0 40px #FFD700;
    -webkit-box-shadow:0 0 40px #FFD700;
    box-shadow:0 0 40px #FFD700;
  }

  50% {
    -moz-box-shadow:0 0 50px #FFD700;
    -webkit-box-shadow:0 0 50px #FFD700;
    box-shadow:0 0 50px #FFD700;
  }

  65% {
    -moz-box-shadow:0 0 90px #FFD700;
    -webkit-box-shadow:0 0 90px #FFD700;
    box-shadow:0 0 90px #FFD700;
  }

  75% {
    -moz-box-shadow:0 0 40px #FFD700;
    -webkit-box-shadow:0 0 40px #FFD700;
    box-shadow:0 0 40px #FFD700;
  }

}



/* Активация анимации */

.btn-primary-buy {

  animation-name: colours;

  animation-timing-function: linear;

  animation-iteration-count: infinite;

  animation-duration: 10s;

}

.inspects_img{
  width:25px;
  vertical-align: middle;
}
#inspects_wrapper{
  margin-left: 20px;
  display:none;
}
#inspects{
  font-weight: bold;
  font-size: 175%;
  vertical-align: middle;
}


</style>


<!-- Media ads -->
<!--
<script src="https://ad.mail.ru/static/vkadman.min.js"></script>
<script src="https://js.appscentrum.com/scr/preroll.js"></script>
<script type="text/javascript">
VK.init( function(){
var user_id = null;
var app_id = 4985742;
var a = new VKAdman();
a.setupPreroll(app_id);
admanStat(app_id, user_id);
});
</script>
-->

 <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix" style="margin-top:10px;">


            <!-- Vk banners ads -->
            <!--
            <div id="vk_ads_76209"></div>
            <script type="text/javascript">
              setTimeout(function() {
                var adsParams = {"ad_unit_id":76209,"ad_unit_hash":"336333b18b48e53386abf74773e0de75"};
                function vkAdsInit() {
                  VK.Widgets.Ads('vk_ads_76209', {}, adsParams);
                }
                if (window.VK && VK.Widgets) {
                  vkAdsInit();
                } else {
                  if (!window.vkAsyncInitCallbacks) window.vkAsyncInitCallbacks = [];
                  vkAsyncInitCallbacks.push(vkAdsInit);
                  var protocol = ((location.protocol === 'https:') ? 'https:' : 'http:');
                  var adsElem = document.getElementById('vk_ads_76209');
                  var scriptElem = document.createElement('script');
                  scriptElem.type = 'text/javascript';
                  scriptElem.async = true;
                  scriptElem.src = protocol + '//vk.com/js/api/openapi.js?125';
                  adsElem.parentNode.insertBefore(scriptElem, adsElem.nextSibling);
                }
              }, 0);
            </script>
            -->



            <!-- Payment view
            <button type="button" class="btn btn-success btn-buy btn-xs" data-toggle="modal" data-target="#select_buy_modal">
              Добавить проверок
            </button>
            &nbsp;
            <span id="inspects_wrapper">
              <img class="inspects_img" src="/img/search_icon.png"/>
              &nbsp;<span id="inspects">Не загружено</span>
            </span>
            -->
            <div class="inner " style="margin-top:5px;">
              <a id="add_app_left_menu" style="display:none;" href="#">Добавить в левое меню</a>
              &nbsp;
              <button type="button" id="invite_friends" class="btn btn-default btn-xs">
                Пригласить друзей
              </button>
              &nbsp;
              <h1 class="cover-heading">Фейкомер</h1>

            </div>
          </div>

          <div class="inner cover">

            <p class="lead">Введите id пользователя или псевдоним страницы</p>
            <p class="lead">
            	<input style="margin-bottom:20px;" id="research_profile" style="color:#292929;" type="text" placeholder="id312314113 или vasyapupkin">

            </p>

            <p class="lead"><button href="#" id="research_start" class="btn btn-lg btn-default">Исследовать</button></p>
            <p class="lead" id="research_result"></p>
            <p class="lead" id="callbacks"></p>
          </div>



        </div>

      </div>

    </div>


    <div class="modal fade"  id="select_buy_modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Пополнить счёт</h4>
          </div>
          <div class="modal-body">

            <table class="packs-block">
              <tbody>
                <tr class="pack product-coin">

                  <td class="pack-left-part">
                    <div class="pack-left-part-inner">
                      <div class="pack-title product-coin">10 <img class="searching_money" src="/img/search_icon.png"></div>
                    </div>
                  </td>

                  <td class="pack-center-part">
                    <div class="pack-price">10 голосов</div>
                  </td>

                  <td class="pack-right-part">
                    <button type="button" class="btn btn-success btn-buy" buy_id="10">Купить</button>
                  </td>

                </tr>

                <tr class="pack product-coin" style="background-color:#f6f6f6;">
                  <td class="pack-left-part" style="border-radius:10px 0 0 10px;">
                    <div class="pack-popular">Популярный</div>
                    <div class="pack-left-part-inner">
                      <div class="pack-title product-coin">30 <img class="searching_money" src="/img/search_icon.png"></div>
                    </div>
                  </td>

                  <td class="pack-center-part">
                    <div class="pack-price">20 голосов</div>
                    <div class="pack-saving">33% скидка</div>
                  </td>

                  <td class="pack-right-part"  style="border-radius:0 10px 10px 0;">
                    <button type="button" class="btn-primary-buy btn btn-success btn-buy" buy_id="30">Купить</button>
                  </td>

                </tr>

                <tr class="pack product-coin">
                  <td class="pack-left-part">
                    <div class="pack-left-part-inner">
                      <div class="pack-title product-coin">50 <img class="searching_money" src="/img/search_icon.png"></div>
                    </div>
                  </td>

                  <td class="pack-center-part">
                    <div class="pack-price">30 голосов</div>
                    <div class="pack-saving">40% скидка</div>
                  </td>

                  <td class="pack-right-part">
                    <button type="button" class="btn btn-success btn-buy" buy_id="50">Купить</button>
                  </td>
                </tr>

                <tr class="pack product-coin">
                  <td class="pack-left-part">
                    <div class="pack-benefit">Выгодный</div>
                    <div class="pack-left-part-inner">
                      <div class="pack-title product-coin">300 <img class="searching_money" src="/img/search_icon.png"></div>
                    </div>
                  </td>

                  <td class="pack-center-part">
                    <div class="pack-price">150 голосов</div>
                    <div class="pack-saving">50% скидка</div>
                  </td>

                  <td class="pack-right-part">
                    <button type="button" class="btn btn-success btn-buy" buy_id="300">Купить</button>
                  </td>

                </tr>
                <!--
                <tr class="pack product-coin" >
                  <td class="pack-left-part" colspan="3" style="padding-top:50px;">
                    <button type="button" id="show_offers_button" class="btn btn-success">Получить <img class="searching_money" src="/img/search_icon.png"> за выполнение заданий</button>
                  </td>
                </tr>
                -->
              </tbody>
            </table>

          </div>

        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection
